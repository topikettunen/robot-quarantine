FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt quarantine.py entrypoint.sh ./

RUN chmod +x entrypoint.sh

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT [ "/usr/src/app/entrypoint.sh" ]
