from bson.json_util import dumps
from bottle import Bottle, get, post, delete, request, response, route
from pymongo import MongoClient, errors

class RobotQuarantine:
    class CORS:
        @classmethod
        def enable(self, func):
            def _enable_cors(*args, **kwargs):
                response.headers['Access-Control-Allow-Origin'] = '*'
                response.headers['Access-Control-Allow-Methods'] = 'GET, POST, DELETE, OPTIONS'
                response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type'
                if request.method != 'OPTIONS':
                    return func(*args, **kwargs)
            return _enable_cors

    def __init__(self, server_ip, server_port, mongo_uri):
        self.app = Bottle()
        self.host_ip = server_ip
        self.host_port = server_port
        self.mongo_uri = mongo_uri
        self.db = MongoClient(self.mongo_uri).quarantine

    @CORS.enable
    def list_quarantined_tests(self):
        response.status = 200
        response.content_type = 'application/json'
        results = self.db.uuids.distinct('uuid')
        return dumps({'quarantined_tests': results})

    def _validate_json(self, json):
        if not json:
            raise ValueError
        for key in json:
            if not key:
                raise ValueError

    @CORS.enable
    def add_test_to_quarantine(self):
        try:
            self._validate_json(request.json)
            result = self.db.uuids.insert(request.json)
            response.status = 200
            return dumps(result)
        except errors.WriteError:
            response.status = 400
            return dumps({'error': 'could not write to database'})
        except ValueError:
            response.status = 400
            return dumps({'error': 'malformed json'})

    @CORS.enable
    def delete_test_from_quarantine(self):
        try:
            self._validate_json(request.json)
            result = self.db.uuids.remove(request.json)
            response.status = 200
            return dumps(result)
        except errors.WriteError:
            response.status = 400
            return dumps({'error': 'could not write to database'})
        except ValueError:
            response.status = 400
            return dumps({'error': 'malformed json'})

    def run(self):
        self.app.route('/uuids',
                       method=['OPTIONS', 'GET'],
                       callback=self.list_quarantined_tests)
        self.app.route('/uuids',
                       method=['OPTIONS', 'POST'],
                       callback=self.add_test_to_quarantine)
        self.app.route('/uuids',
                       method=['OPTIONS', 'DELETE'],
                       callback=self.delete_test_from_quarantine)
        self.app.run(server='tornado', host=self.host_ip, port=self.host_port)

if __name__ == '__main__':
    import os
    
    mongo_uri = 'mongodb://{}:{}@{}'.format(os.environ['MONGO_USER'],
                                            os.environ['MONGO_PASSWORD'],
                                            os.environ['MONGO_HOST'])
    app = RobotQuarantine('0.0.0.0', 8080, mongo_uri)
    app.run()
